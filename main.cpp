#include "targetsmanager.h"

using namespace std;

int main(int argc, char* args[])
{
    if(argc < 2)
    {
        cout << "Too few params" << endl;
        return 1;
    }
    if(argc > 2)
    {
        cout << "Too many params" << endl;
        return 1;
    }

    TargetsManager manager("makefile2.txt");
    manager.setGoalTargetName(args[1]);
    return manager.resolveGoalTarget();
}

