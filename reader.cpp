#include "reader.h"
#include <stdlib.h> //exit

bool Reader::isCharSym(char ch)
{
    return (((ch >= '0') && (ch <= '9')) || ((ch >= 'a') && (ch <= 'z')) || ((ch >= 'A') && (ch <= 'Z')) || ch == '_');
}

bool Reader::isSpaceSym(char ch)
{
    return ((ch == ' ') || (ch == '\t'));
}

Reader::Reader(const char* filename)
{
    lineNumber = 1;
    file = new ifstream(filename, ios::binary);
    if(!(*file))
    {
        cout << "File not found!";
        exit(EXIT_FAILURE);
    }
    charList.resize(0);
    charString = 0;
}

Reader::~Reader()
{
    if(charString)
        delete [] charString;
    if (file)
        delete file;
}

Reader &Reader::operator +(char ch)
{
    charList.push_back(ch);
    wasCharListModified = true;
    return *this;
}

int Reader::skeepSpaces()
{
    int spacesCount = 0;
    char ch = ' ';
    ch = file->get();
    while(isSpaceSym(ch))
    {
        ++spacesCount;
        ch = file->get();
    }
    file->seekg(-1, ios::cur);
    return spacesCount;

}

void Reader::skeepNewlines()
{
    char ch = ' ';
    ch = file->get();
    while((ch == '\r') || (ch == '\n'))
    {
        ch = file->get();
    }
    file->seekg(-1, ios::cur);
    lineNumber++;
}

Reader &Reader::readAWord()
{
    charList.clear();
    int charsCount = 0;
    char ch = 'a';
    ch = file->get();
    while(isCharSym(ch))
    {
        (*this) + ch;
        ++charsCount;
        ch = file->get();
    }
    file->seekg(-1, ios::cur);
    if(!charsCount)
        reportForAnError("Expects char symbols, but not!", ch);
    return *this;
}

Reader &Reader::readALine()
{
    charList.clear();
    int charsCount = 0;
    char ch = 'a';
    ch = file->get();
    while(ch != '\n' && !isEOF())
    {
        (*this) + ch;
        ++charsCount;
        ch = file->get();
    }
    return *this;
    lineNumber++;
}

void Reader::readAColon()
{
    char ch;
    ch = file->get();
    if(ch != ':')
        reportForAnError("Expects ':' symbol, but not!", ch);
}

bool Reader::isEOF()
{
    return (file->peek() == -1);
}

int Reader::charCount()
{
    return charList.size();
}

void Reader::reportForAnError(const char *msg, char ch)
{
    cerr << endl << msg << endl << "Line: " << lineNumber << endl << ch << (int)ch;
    exit(EXIT_FAILURE);
}

Reader::operator char *()
{
    if(wasCharListModified && charString)
        delete [] charString;
    if(!charList.empty())
    {
        charString = new char[charList.size()+1];
        std::list<char>::const_iterator it = charList.begin();
        int pos = 0;
        for(;it != charList.end(); ++it, ++pos)
        {
            charString[pos] = *it;
        }
        charString[pos] = 0;
        return charString;
    }
    else
        return 0;
}

