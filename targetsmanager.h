#ifndef TARGETSMANAGER_H
#define TARGETSMANAGER_H

#include <list>
#include <string>
#include "targetnode.h"
#include "reader.h"

#include <iostream>

using namespace std;

class TargetsManager
{
    list<TargetNode*> listNodes;
    string goalTargetName;
    Reader* reader;
    list<TargetNode*> targetsList;

    TargetNode* findTargetByName(string);
    bool isListContainsTarget(list<TargetNode*> &list, TargetNode* node);

    void formTargetsList();
    bool formTargetsDepends();
    bool createDepTreeForGoalTarget();
    bool addTargetInRunningList(list<TargetNode*> &pathList, TargetNode* node);
    void runTasksOfTargets();
public:
    TargetsManager(const char* filename);
    ~TargetsManager();

    string getGoalTargetName() const;
    void setGoalTargetName(string value);
    int resolveGoalTarget();
};

#endif // TARGETSMANAGER_H
