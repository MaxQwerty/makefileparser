#ifndef READER_H
#define READER_H

#include <list>
#include <iostream>
#include <fstream>

using namespace std;

class Reader
{
    list<char> charList;
    char* charString;
    bool wasCharListModified;
    ifstream* file;
    int lineNumber;

    bool isCharSym(char ch);
    bool isSpaceSym(char ch);
    void reportForAnError(const char *msg, char ch='!');
public:
    Reader(const char* filename);
    ~Reader();
    Reader& operator + (char ch);
    operator char* ();

    int skeepSpaces();
    void skeepNewlines();
    Reader& readAWord();
    Reader& readALine();
    void readAColon();
    bool isEOF();
    int charCount();
};

#endif // READER_H
