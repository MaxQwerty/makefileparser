#include "targetsmanager.h"
#include <stdlib.h> //exit

string TargetsManager::getGoalTargetName() const
{
    return goalTargetName;
}

void TargetsManager::setGoalTargetName(string value)
{
    goalTargetName = value;
    cout << "Working for " << goalTargetName << endl;
}

void TargetsManager::formTargetsList()
{
    while (!reader->isEOF()) {
        reader->skeepNewlines();
        if(reader->isEOF())
            break;
        char *target = reader->readAWord();
        TargetNode *targetNode = new TargetNode();
        listNodes.push_back(targetNode);
        targetNode->setName(target);
        reader->readAColon();
        while (reader->skeepSpaces() != 0)
        {
            string dependence(reader->readAWord());
            targetNode->addDependenceName(dependence);
        }
        reader->skeepNewlines();
        while (reader->skeepSpaces() != 0 && !reader->isEOF())
        {
            char *task = reader->readALine();
            targetNode->addTask(task);
        }
    }
}

bool TargetsManager::formTargetsDepends()
{
    list<TargetNode* >::iterator itTargets = listNodes.begin();
    TargetNode* bufNode;
    string bufName;
    for(; itTargets != listNodes.end(); ++itTargets)
    {
        bufNode = *itTargets;
        list<string> depNames = bufNode->DependenceNames();
        list<string>::iterator itDependsNames = depNames.begin();
        for(; itDependsNames != depNames.end(); ++itDependsNames)
        {
            bufName = *(itDependsNames);
            TargetNode* bufDepNode = findTargetByName(bufName);
            if(bufDepNode)
            {
                bufNode->addDepNode(bufDepNode);
            }
            else
            {
                cerr << "Missing dependence rule " << bufName << " for " << bufNode->getName() << endl;
                return false;
            }
        }
    }
    return true;
}

bool TargetsManager::createDepTreeForGoalTarget()
{
    TargetNode* goalNode = findTargetByName(goalTargetName);
    if(!goalNode)
    {
        cerr << "Goal name " << goalTargetName << " not found in makefile" << endl;
        return false;
    }
    list<TargetNode*> listForPathMonitoring;
    return addTargetInRunningList(listForPathMonitoring, goalNode);
}

void TargetsManager::runTasksOfTargets()
{
    list<TargetNode*>::iterator it = targetsList.begin();
    for(; it !=targetsList.end(); ++it)
    {
        cout << (*it)->getName() << ':' << endl;
        queue<string> tasks = (*it)->getTasks();
        int tasksNumber = 0;
        while(!tasks.empty())
        {
            cout << "runnng task " << ++tasksNumber << ": " << tasks.front() << endl;
            cout << "return: " << system(tasks.front().c_str()) << endl << endl;
            tasks.pop();
        }
    }
}

int TargetsManager::resolveGoalTarget()
{
    formTargetsList();
    if(!formTargetsDepends())
        return 1;
    if(!createDepTreeForGoalTarget())
        return 1;
    runTasksOfTargets();
    cout << "Done." << endl;
    return 0;
}

bool TargetsManager::addTargetInRunningList(list<TargetNode*> &pathList, TargetNode* tgtNode)
{
    if(isListContainsTarget(pathList, tgtNode))
    {
        cerr << "Detected loop in " << tgtNode->getName() << endl;
        return false;
    }
    pathList.push_front(tgtNode);
    bool result = true;
    list<TargetNode *> deps = tgtNode->dependences();
    list<TargetNode*>::iterator it = deps.begin();
    for(; it != deps.end(); ++it)
    {
        if(!result)
            break;
        if(!isListContainsTarget(targetsList, *it))
            result &= addTargetInRunningList(pathList, *it);
    }
    pathList.pop_front();
    targetsList.push_back(tgtNode);
    return result;
}

bool TargetsManager::isListContainsTarget(list<TargetNode*> &listNodes,TargetNode *node)
{
    list<TargetNode* >::iterator it = listNodes.begin();
    for(; it !=listNodes.end(); ++it)
    {
        if((*it) == node)
            return true;
    }
    return false;
}

TargetNode *TargetsManager::findTargetByName(string name)
{
    list<TargetNode*>::iterator itNode = listNodes.begin();
    for(; itNode != listNodes.end(); ++itNode)
    {
        if(name == (*itNode)->getName())
        {
            return *itNode;
        }
    }
    return 0;
}

TargetsManager::TargetsManager(const char *filename)
{
    reader = new Reader(filename);
}

TargetsManager::~TargetsManager()
{
    if(reader)
        delete reader;
    std::list<TargetNode *>::iterator it = listNodes.begin();
    for(;it != listNodes.end(); ++it)
    {
        if(*it)
            delete (*it);
    }
}

