#include "targetnode.h"


string TargetNode::getName() const
{
    return name;
}

void TargetNode::setName(const string n)
{
    name = n;
}

TargetNode::TargetNode()
{

}

TargetNode::~TargetNode()
{
    std::list<TargetNode *>::iterator it = listDependences.begin();
}

void TargetNode::addTask(string taskStr)
{
    queueTasks.push(taskStr);
}

queue<string> &TargetNode::getTasks()
{
    return queueTasks;
}

void TargetNode::addDepNode(TargetNode *node)
{
    listDependences.push_front(node);
}

list<TargetNode *> &TargetNode::dependences()
{
    return listDependences;
}

void TargetNode::addDependenceName(string depName)
{
    listDependencesNames.push_front(depName);
}

list<string> &TargetNode::DependenceNames()
{
    return listDependencesNames;
}

