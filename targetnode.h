#ifndef TARGETNODE_H
#define TARGETNODE_H

#include <list>
#include <queue>
#include <string>

using namespace std;

class TargetNode
{
    queue<string> queueTasks;
    list<TargetNode *> listDependences;
    list<string> listDependencesNames;
    string name;
public:
    TargetNode();
    ~TargetNode();

    void addTask(string);
    queue<string> &getTasks();

    void addDepNode(TargetNode *);
    list<TargetNode *> &dependences();

    void addDependenceName(string);
    list<string> &DependenceNames();

    string getName() const;
    void setName(const string);
};

#endif // TARGETNODE_H
